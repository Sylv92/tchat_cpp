#ifndef WINSTART_H
#define WINSTART_H

#include <QtWidgets>
#include <QtNetwork>
#include "ui_WinStart.h"

class WinStart : public QWidget, private Ui::WinStart
{
    Q_OBJECT

    public:
        WinStart();

    private slots:
        void on_buttonConnect_clicked();

};

#endif // WINSTART_H
