#include <QApplication>
#include "WinStart.h"

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    WinStart window;
    window.show();

    return app.exec();
}
