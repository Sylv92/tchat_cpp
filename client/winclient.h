#ifndef WINCLIENT_H
#define WINCLIENT_H

#include <QtWidgets>
#include <QtNetwork>
#include "ui_WinClient.h"

class WinClient : public QWidget, private Ui::WinClient
{
    Q_OBJECT

    public:
        WinClient(int port, QString address, QString pseudo);

    private slots:
        void connectToServer();
        void on_buttonSend_clicked();
        void on_inputMessage_returnPressed();
        void on_buttonChange_clicked();
        void dataReceived();
        void connectedSlot();
        void deconnectedSlot();
        void errorSocket(QAbstractSocket::SocketError error);

    private:
        QTcpSocket *socket;
        quint16 messageSize;
        int port;
        QString address;
        QString pseudo;
};

#endif // WINCLIENT_H
