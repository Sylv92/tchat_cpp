#include "WinClient.h"
#include "WinStart.h"

WinClient::WinClient(int inputPort, QString inputAddress, QString inputPseudo) {
    setupUi(this);

    port = inputPort;
    address = inputAddress;
    pseudo = inputPseudo;

    socket = new QTcpSocket(this);
    connect(socket, SIGNAL(readyRead()), this, SLOT(dataReceived()));
    connect(socket, SIGNAL(connected()), this, SLOT(connectedSlot()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(deconnectedSlot()));
    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(errorSocket(QAbstractSocket::SocketError)));

    messageSize = 0;
    connectToServer();
}

void WinClient::connectToServer() {
    messagesArea->append("<em>Trying to connect on the server ...</em>");
    socket->abort();
    socket->connectToHost(address, port);

}

void WinClient::on_buttonChange_clicked() {
    WinStart *winStart = new WinStart();
    winStart->show();
    this->close();
}

void WinClient::on_buttonSend_clicked() {
    QByteArray packet;
    QDataStream out(&packet, QIODevice::WriteOnly);
    QString getMessage = inputMessage->text();

    QString message = "<b>" + pseudo  + " : </b>" + getMessage;

    out << (quint16) 0;
    out << message;
    out.device()->seek(0);
    out << (quint16)(packet.size() - sizeof(quint16));

    socket->write(packet);

    inputMessage->clear();
    inputMessage->setFocus();
}

void WinClient::on_inputMessage_returnPressed() {
    on_buttonSend_clicked();
}

void WinClient::dataReceived() {
    QDataStream in(socket);
    if (messageSize == 0) {
        if(socket->bytesAvailable() < (int)sizeof(quint16)) {
            return;
        }
        in >> messageSize;
    }
    if (socket->bytesAvailable() < messageSize) {
        return;
    }

    QString messageReceived;
    in >> messageReceived;

    if (messageReceived.mid(0, messageReceived.indexOf("|")) == "users") {
        usersArea->clear();
        usersArea->append(messageReceived.mid(messageReceived.indexOf("|") + 1, messageReceived.length()));
        messageSize = 0;
        return;
    }

    messagesArea->append(messageReceived);

    messageSize = 0;
}

void WinClient::connectedSlot() {
    messagesArea->append("<em>Connection succeeded</em>");
    QByteArray packet;
    QDataStream out(&packet, QIODevice::WriteOnly);

    QString message = "new|" + pseudo;

    out << (quint16) 0;
    out << message;
    out.device()->seek(0);
    out << (quint16)(packet.size() - sizeof(quint16));

    socket->write(packet);
}

void WinClient::deconnectedSlot() {
    messagesArea->append("<em>Deconnected from the Server</em>");
}

void WinClient::errorSocket(QAbstractSocket::SocketError error) {
    switch (error) {
    case QAbstractSocket::HostNotFoundError:
                messagesArea->append("<em>Couldn't find the server</em>");
                break;
            case QAbstractSocket::ConnectionRefusedError:
                messagesArea->append("<em>Connection has been refused</em>");
                break;
            case QAbstractSocket::RemoteHostClosedError:
                messagesArea->append("<em>Lost connection to the server</em>");
                break;
            default:
                messagesArea->append("<em>Error : " + socket->errorString() + "</em>");
    }
}
