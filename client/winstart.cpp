#include "WinStart.h"
#include "WinClient.h"

WinStart::WinStart() {
    setupUi(this);

}

void WinStart::on_buttonConnect_clicked() {
    QString port = inputPort->text();
    QString address = inputAddress->text();
    QString pseudo = inputPseudo->text();

    if((!port.isNull() || !port.isEmpty()) &&
        (!address.isNull() || !address.isEmpty()) &&
        (!pseudo.isNull() || !pseudo.isEmpty())) {
        WinClient *winClient = new WinClient(port.toInt(), address, pseudo);
        winClient->show();
        this->hide();
    }
}


