TEMPLATE = app
QT += widgets network
DEPENDPATH += .
INCLUDEPATH += .

SOURCES += \
    main.cpp \
    WinClient.cpp \
    WinStart.cpp

HEADERS += \
    WinClient.h \
    WinStart.h

FORMS += \
    WinClient.ui \
    WinStart.ui
