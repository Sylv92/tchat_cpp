TEMPLATE = app
QT += widgets network
DEPENDPATH += .
INCLUDEPATH += .

SOURCES += \
    main.cpp \
    WinServer.cpp \
    client.cpp

HEADERS += \
    WinServer.h \
    client.h
