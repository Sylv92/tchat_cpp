#include "client.h"

Client::Client(QTcpSocket *_clientSocket)
{
    clientSocket = _clientSocket;
}

QString Client::getName() {
    return name;
}

void Client::setName(QString _name) {
    name = _name;
}

QTcpSocket *Client::getClientSocket() {
    return clientSocket;
}
