#ifndef WINSERVER_H
#define WINSERVER_H

#include <QtWidgets>
#include <QtNetwork>
#include <QTextStream>
#include <QFile>
#include "client.h"


class WinServer : public QWidget
{
    Q_OBJECT

    public:
        WinServer();
        void sendAll(const QString &message);
        void writeFile(QString message);
        void sendToUser(QTcpSocket *newClient);
        void preparePacket(QTcpSocket *newClient, QString message);
        void updateUsers();

    private slots:
        void connection();
        void dataReceived();
        void deconnection();

    private:
        QLabel *serverState;
        QPushButton *buttonQuit;

        QTcpServer *server;
        QList<Client> clients;
        quint16 messageSize;

        QString fileName = "save_chat.txt";
};

#endif
