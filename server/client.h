#ifndef CLIENT_H
#define CLIENT_H
#include <QtNetwork>

class Client
{
public:
    Client(QTcpSocket* clientSocket);
    QString getName();
    QTcpSocket* getClientSocket();
    void setName(QString _name);

private:
    QString name;
    QTcpSocket *clientSocket;

};

#endif // CLIENT_H
