#include <QApplication>
#include "WinServer.h"

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    WinServer window;
    window.show();

    return app.exec();
}
