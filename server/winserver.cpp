#include "WinServer.h"
#include "client.h"

WinServer::WinServer()
{
    serverState = new QLabel;
    buttonQuit = new QPushButton("Quit");
    connect(buttonQuit, SIGNAL(clicked()), qApp, SLOT(quit()));

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(serverState);
    layout->addWidget(buttonQuit);
    setLayout(layout);

    setWindowTitle("Server");

    server = new QTcpServer(this);
    if(!server->listen(QHostAddress::Any, 50885)){
        serverState->setText("Couldn't start server :" + server->errorString());
    } else {
        serverState->setText("Server has started on port " + QString::number(server->serverPort()));
        connect(server, SIGNAL(newConnection()), this, SLOT(connection()));
    }

    messageSize = 0;
}

void WinServer::connection() {
    sendAll("<em>A new user has just connected on the server</em>");
    /*QString test = "BONJOUR";
    test.mid(0, test.indexOf(":"));
    */
    QTcpSocket *newClient = server->nextPendingConnection();
    Client client = Client(newClient);
    clients << client;

    sendToUser(client.getClientSocket());

    connect(newClient, SIGNAL(readyRead()), this, SLOT(dataReceived()));
    connect(newClient, SIGNAL(disconnected()), this, SLOT(deconnection()));
}


void WinServer::sendToUser(QTcpSocket *newClient) {
    QFile file(fileName);
    if(file.exists()) {
        QString content;
        if(file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QTextStream stream(&file);
            content.append(stream.readAll());
            QByteArray packet;
            QDataStream out(&packet, QIODevice::WriteOnly);
            out << (quint16) 0;
            out << content;
            out.device()->seek(0);
            out << (quint16) (packet.size() - sizeof(quint16));
            newClient->write(packet);
        }
    }
    file.close();
}

void WinServer::updateUsers() {
    QString users = "users|";
    for(int i = 0; i < clients.size(); i++) {
        users += clients[i].getName() + "<br/>";
    }
    sendAll(users);
}

void WinServer::dataReceived() {

    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    if (socket == 0) {
        return;
    }
    QDataStream in(socket);
    if(messageSize == 0) {
        if(socket->bytesAvailable() < (int)sizeof(quint16)) {
            return;
        }
        in >> messageSize;
    }
    if (socket->bytesAvailable() < messageSize) {
        return;
    }
    QString message;
    in >> message;

    if(message.mid(0, message.indexOf("|")) == "new") {
        qInfo() << "compare success";
        int size = clients.size();
        QString name = message.mid(message.indexOf("|") + 1, message.length());
        qInfo() << name;
        clients[size - 1].setName(name);
        messageSize = 0;
        updateUsers();
        return;
    }

    writeFile(message);
    sendAll(message);

    messageSize = 0;
}

void WinServer::writeFile(QString message) {
    QFile file(fileName);
    file.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream stream(&file);
    stream << message << "<br/>";
    file.close();
}

void WinServer::deconnection() {
    sendAll("<em>An user has just deconnected from the server</em>");

    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    if (socket == 0) {
        return;
    }
    for(int i = 0; i < clients.size(); i++) {
        if (clients[i].getClientSocket() == socket) {
            clients.removeAt(i);
        }
    }
    updateUsers();
    socket->deleteLater();
}

void WinServer::sendAll(const QString &message) {
    QByteArray packet;
    QDataStream out(&packet, QIODevice::WriteOnly);

    out << (quint16) 0;
    out << message;
    out.device()->seek(0);
    out << (quint16) (packet.size() - sizeof(quint16));

    for (int i = 0; i < clients.size(); i++) {
        clients[i].getClientSocket()->write(packet);
    }
}
