/********************************************************************************
** Form generated from reading UI file 'WinClient.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINCLIENT_H
#define UI_WINCLIENT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WinClient
{
public:
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *inputMessage;
    QPushButton *buttonSend;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *buttonChange;
    QSpacerItem *horizontalSpacer;
    QWidget *horizontalLayoutWidget_3;
    QHBoxLayout *horizontalLayout_3;
    QTextBrowser *messagesArea;
    QTextEdit *usersArea;

    void setupUi(QWidget *WinClient)
    {
        if (WinClient->objectName().isEmpty())
            WinClient->setObjectName(QStringLiteral("WinClient"));
        WinClient->resize(470, 332);
        horizontalLayoutWidget_2 = new QWidget(WinClient);
        horizontalLayoutWidget_2->setObjectName(QStringLiteral("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(10, 280, 451, 41));
        horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(horizontalLayoutWidget_2);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_2->addWidget(label_2);

        inputMessage = new QLineEdit(horizontalLayoutWidget_2);
        inputMessage->setObjectName(QStringLiteral("inputMessage"));
        inputMessage->setEnabled(true);
        inputMessage->setFrame(true);

        horizontalLayout_2->addWidget(inputMessage);

        buttonSend = new QPushButton(horizontalLayoutWidget_2);
        buttonSend->setObjectName(QStringLiteral("buttonSend"));

        horizontalLayout_2->addWidget(buttonSend);

        horizontalLayoutWidget = new QWidget(WinClient);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(9, 9, 451, 41));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        buttonChange = new QPushButton(horizontalLayoutWidget);
        buttonChange->setObjectName(QStringLiteral("buttonChange"));

        horizontalLayout->addWidget(buttonChange);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        horizontalLayoutWidget_3 = new QWidget(WinClient);
        horizontalLayoutWidget_3->setObjectName(QStringLiteral("horizontalLayoutWidget_3"));
        horizontalLayoutWidget_3->setGeometry(QRect(9, 59, 451, 211));
        horizontalLayout_3 = new QHBoxLayout(horizontalLayoutWidget_3);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        messagesArea = new QTextBrowser(horizontalLayoutWidget_3);
        messagesArea->setObjectName(QStringLiteral("messagesArea"));

        horizontalLayout_3->addWidget(messagesArea);

        usersArea = new QTextEdit(horizontalLayoutWidget_3);
        usersArea->setObjectName(QStringLiteral("usersArea"));

        horizontalLayout_3->addWidget(usersArea);


        retranslateUi(WinClient);

        QMetaObject::connectSlotsByName(WinClient);
    } // setupUi

    void retranslateUi(QWidget *WinClient)
    {
        WinClient->setWindowTitle(QApplication::translate("WinClient", "Dialog", nullptr));
        label_2->setText(QApplication::translate("WinClient", "Message :", nullptr));
        buttonSend->setText(QApplication::translate("WinClient", "send", nullptr));
        buttonChange->setText(QApplication::translate("WinClient", "Change Server", nullptr));
    } // retranslateUi

};

namespace Ui {
    class WinClient: public Ui_WinClient {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINCLIENT_H
