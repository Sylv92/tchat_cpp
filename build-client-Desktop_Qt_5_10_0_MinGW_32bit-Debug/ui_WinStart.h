/********************************************************************************
** Form generated from reading UI file 'WinStart.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINSTART_H
#define UI_WINSTART_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WinStart
{
public:
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *inputPort;
    QLabel *label_2;
    QLineEdit *inputAddress;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_3;
    QLineEdit *inputPseudo;
    QSpacerItem *horizontalSpacer;
    QPushButton *buttonConnect;

    void setupUi(QWidget *WinStart)
    {
        if (WinStart->objectName().isEmpty())
            WinStart->setObjectName(QStringLiteral("WinStart"));
        WinStart->resize(400, 162);
        horizontalLayoutWidget = new QWidget(WinStart);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(9, 9, 381, 71));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(horizontalLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        inputPort = new QLineEdit(horizontalLayoutWidget);
        inputPort->setObjectName(QStringLiteral("inputPort"));

        horizontalLayout->addWidget(inputPort);

        label_2 = new QLabel(horizontalLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout->addWidget(label_2);

        inputAddress = new QLineEdit(horizontalLayoutWidget);
        inputAddress->setObjectName(QStringLiteral("inputAddress"));

        horizontalLayout->addWidget(inputAddress);

        horizontalLayoutWidget_2 = new QWidget(WinStart);
        horizontalLayoutWidget_2->setObjectName(QStringLiteral("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(9, 89, 381, 51));
        horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_3 = new QLabel(horizontalLayoutWidget_2);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_2->addWidget(label_3);

        inputPseudo = new QLineEdit(horizontalLayoutWidget_2);
        inputPseudo->setObjectName(QStringLiteral("inputPseudo"));

        horizontalLayout_2->addWidget(inputPseudo);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        buttonConnect = new QPushButton(horizontalLayoutWidget_2);
        buttonConnect->setObjectName(QStringLiteral("buttonConnect"));

        horizontalLayout_2->addWidget(buttonConnect);


        retranslateUi(WinStart);

        QMetaObject::connectSlotsByName(WinStart);
    } // setupUi

    void retranslateUi(QWidget *WinStart)
    {
        WinStart->setWindowTitle(QApplication::translate("WinStart", "Form", nullptr));
        label->setText(QApplication::translate("WinStart", "Port :", nullptr));
        label_2->setText(QApplication::translate("WinStart", "Address : ", nullptr));
        label_3->setText(QApplication::translate("WinStart", "Pseudo :", nullptr));
        buttonConnect->setText(QApplication::translate("WinStart", "Connect", nullptr));
    } // retranslateUi

};

namespace Ui {
    class WinStart: public Ui_WinStart {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINSTART_H
