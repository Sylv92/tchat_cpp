/********************************************************************************
** Form generated from reading UI file 'WinClient.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINCLIENT_H
#define UI_WINCLIENT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WinClient
{
public:
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *inputMessage;
    QPushButton *buttonSend;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout_3;
    QTextEdit *messagesArea;

    void setupUi(QWidget *WinClient)
    {
        if (WinClient->objectName().isEmpty())
            WinClient->setObjectName(QStringLiteral("WinClient"));
        WinClient->resize(470, 332);
        horizontalLayoutWidget_2 = new QWidget(WinClient);
        horizontalLayoutWidget_2->setObjectName(QStringLiteral("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(10, 280, 451, 41));
        horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(horizontalLayoutWidget_2);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_2->addWidget(label_2);

        inputMessage = new QLineEdit(horizontalLayoutWidget_2);
        inputMessage->setObjectName(QStringLiteral("inputMessage"));
        inputMessage->setEnabled(false);
        inputMessage->setFrame(true);

        horizontalLayout_2->addWidget(inputMessage);

        buttonSend = new QPushButton(horizontalLayoutWidget_2);
        buttonSend->setObjectName(QStringLiteral("buttonSend"));

        horizontalLayout_2->addWidget(buttonSend);

        verticalLayoutWidget = new QWidget(WinClient);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(9, 9, 451, 261));
        verticalLayout_3 = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        messagesArea = new QTextEdit(verticalLayoutWidget);
        messagesArea->setObjectName(QStringLiteral("messagesArea"));

        verticalLayout_3->addWidget(messagesArea);


        retranslateUi(WinClient);

        QMetaObject::connectSlotsByName(WinClient);
    } // setupUi

    void retranslateUi(QWidget *WinClient)
    {
        WinClient->setWindowTitle(QApplication::translate("WinClient", "Dialog", nullptr));
        label_2->setText(QApplication::translate("WinClient", "Message :", nullptr));
        buttonSend->setText(QApplication::translate("WinClient", "send", nullptr));
    } // retranslateUi

};

namespace Ui {
    class WinClient: public Ui_WinClient {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINCLIENT_H
